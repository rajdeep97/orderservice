package com.meru.OrderService.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.meru.OrderService.constant.Constant;
import com.meru.OrderService.service.OrderProcessingService;
@Component
public class QueueOperation {

	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private OrderProcessingService processingService;
	
	public void buildOrderRequest(String jsonOrderRequest)
	{
		this.jmsTemplate.convertAndSend(Constant.ADD_CUSTOMER_TO_ORDER_REQUEST ,jsonOrderRequest);
	}
	
	@JmsListener(destination = Constant.ORDER_REQUEST_READY)
	@SendTo(Constant.PAYMENT_REQUESTED)
	public String getPaymentForOrder(String jsonGeneratedOrderRequest)
	{
		return this.processingService.validateAndGenerateOrder(jsonGeneratedOrderRequest);
	}
	
	@JmsListener(destination = Constant.PAYMENT_SUCCESSFUL)
	@SendTo(Constant.ORDER_CONFIRMED)
	public String confirmAndUpdateOrder(String prepaidOrder)
	{
		return this.processingService.confirmAndUpdateOrder(prepaidOrder);
	}
}
