package com.meru.OrderService.service;

import java.util.List;

import com.meru.OrderService.model.Order;

public interface OrderAPIService {

	public void placeOrderRequest(long customerID);

	public Order getOrderByOrderId(long orderID);

	public List<Order> getOrderByCustomerId(long customerID);

}
