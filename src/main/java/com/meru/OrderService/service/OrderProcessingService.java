package com.meru.OrderService.service;

public interface OrderProcessingService {

	public String validateAndGenerateOrder(String jsonGeneratedOrderRequest);

	public String confirmAndUpdateOrder(String prepaidOrder);

}
