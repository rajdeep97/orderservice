package com.meru.OrderService.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meru.OrderService.dto.OrderRequest;
import com.meru.OrderService.exception.UnableToProcessOrderException;
import com.meru.OrderService.external.model.CartItem;
import com.meru.OrderService.external.model.Customer;
import com.meru.OrderService.external.model.Inventory;
import com.meru.OrderService.external.model.Product;
import com.meru.OrderService.model.Order;
import com.meru.OrderService.model.OrderItem;
import com.meru.OrderService.repository.OrderRepository;
import com.meru.OrderService.service.OrderProcessingService;
@Service
public class OrderProcessingServiceImpl implements OrderProcessingService{
	
	@Autowired
	private OrderRepository repository;
	

	@Override
	public String validateAndGenerateOrder(String jsonGeneratedOrderRequest) {
		ObjectMapper om = new ObjectMapper();
		String jsonNewOrder = null;
		try {
			OrderRequest generatedOrderRequest = om.readValue(jsonGeneratedOrderRequest, OrderRequest.class);
			Order newOrder = new Order();
			Customer cust = generatedOrderRequest.getCustomer();
			newOrder.setCustomerID(generatedOrderRequest.getCustomerID());
			//newOrder.setCustomer(generatedOrderRequest.getCustomer());
			newOrder.setCustomerName(cust.getCustomerName());
			newOrder.setCustomerAddress(cust.getCustomerAddress());
			newOrder.setCustomerEmail(cust.getCustomerEmail());
			newOrder.setCustomerContactNo(cust.getCustomerContactNo());
			newOrder.setOrderDate(new Date());
			newOrder.setOrderConfirmed(false);
			newOrder.setPaymentConfirmed(false);
			
			List<CartItem> itemsInCart = generatedOrderRequest.getCart().getShoppingCart();
			List<Product> productsInCart = generatedOrderRequest.getProducts();
			List<Inventory> inventoriesOfProducts = generatedOrderRequest.getInventories();
			
			if(itemsInCart.size() == 0)
			{
				throw new UnableToProcessOrderException("There are no items in the cart");
			}
			
			if(itemsInCart.size() != productsInCart.size() && itemsInCart.size() != inventoriesOfProducts.size())
			{
				throw new UnableToProcessOrderException("Mismatch in number of items requested and returned.");
			}
			else
			{
				Map<Long,Integer> explodedCart = new HashMap<Long,Integer>();
				Map<Long,Integer> explodedInventories = new HashMap<Long,Integer>();
				Map<Long,Product> addressableProduct = new HashMap<Long,Product>();
				
				for(CartItem item : itemsInCart)
				{
					explodedCart.put(item.getProductID(), item.getProductQuantity());
				}
				
				for(Inventory ivt : inventoriesOfProducts)
				{
					explodedInventories.put(ivt.getProductID(), ivt.getProductInventory());
				}
				
				for(Product product : productsInCart)
				{
					addressableProduct.put(product.getProductID(), product);
				}
				
				for(long productId : explodedCart.keySet())
				{
					int quantityAvailable = explodedInventories.get(productId);
					int quantityRequested = explodedCart.get(productId);
					if(quantityRequested > quantityAvailable)
					{
						throw new UnableToProcessOrderException(addressableProduct.get(productId).getProductName() + " not available in STOCK");
					}
				}
				
				List<OrderItem> orderItems = new ArrayList<OrderItem>();
				for(long productId : explodedCart.keySet())
				{
					OrderItem orderItem = new OrderItem();
					Product prod = addressableProduct.get(productId);
					orderItem.setProductID(productId);
					orderItem.setProductName(prod.getProductName());
					orderItem.setProductPrice(prod.getProductPrice());
					orderItem.setQuantity(explodedCart.get(productId));
					
					orderItems.add(orderItem);
				}
				
				double orderAmount = 0.0;
				
				for(OrderItem orderItem : orderItems)
				{
					double totalpriceofproduct = orderItem.getProductPrice() * orderItem.getQuantity();
					orderAmount+=totalpriceofproduct;
				}
				
				newOrder.setOrderItems(orderItems);
				newOrder.setOrderAmount(orderAmount);
				
				jsonNewOrder = om.writeValueAsString(this.repository.save(newOrder));
			}
		}
		catch(JsonMappingException e)
		{
			e.printStackTrace();
		}
		catch(JsonProcessingException e)
		{
			e.printStackTrace();
		}
		
		return jsonNewOrder;
	}

	@Override
	public String confirmAndUpdateOrder(String prepaidOrder) {
		ObjectMapper om = new ObjectMapper();
		String confirmedOrder = null;
		try {
			Order paymentConfirmedOrder = om.readValue(prepaidOrder, Order.class);
			
			if(paymentConfirmedOrder.isPaymentConfirmed())
			{
				paymentConfirmedOrder.setOrderConfirmed(true);
			}
			
			confirmedOrder = om.writeValueAsString(this.repository.save(paymentConfirmedOrder));
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return confirmedOrder;
		
	}

}
