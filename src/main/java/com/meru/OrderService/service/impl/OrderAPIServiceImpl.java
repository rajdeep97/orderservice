package com.meru.OrderService.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meru.OrderService.dto.OrderRequest;
import com.meru.OrderService.exception.OrderNotFoundException;
import com.meru.OrderService.messaging.QueueOperation;
import com.meru.OrderService.model.Order;
import com.meru.OrderService.repository.OrderRepository;
import com.meru.OrderService.service.OrderAPIService;
@Service
public class OrderAPIServiceImpl implements OrderAPIService{

	@Autowired
	private QueueOperation queueOperation;
	
	@Autowired
	private OrderRepository repository;
	
	@Override
	public void placeOrderRequest(long customerID) {
		OrderRequest orderRequest = new OrderRequest();
		orderRequest.setCustomerID(customerID);
		
		ObjectMapper om = new ObjectMapper();
		String jsonOrderRequest = null;
		try
		{
			jsonOrderRequest = om.writeValueAsString(orderRequest);
		}
		catch(JsonProcessingException exception)
		{
			exception.printStackTrace();
		}
		
		queueOperation.buildOrderRequest(jsonOrderRequest);
		
	}

	@Override
	public Order getOrderByOrderId(long orderID) {
		Optional<Order> order = this.repository.findById(orderID);
		if(order.isPresent())
		{
			return order.get();
		}
		else
		{
			throw new OrderNotFoundException("No Order found with OrderID : "+orderID);
		}
	}

	@Override
	public List<Order> getOrderByCustomerId(long customerID) {
		return this.repository.getOrderByCustomerId(customerID);
	}

}
