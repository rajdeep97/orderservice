package com.meru.OrderService.constant;

public final class Constant {

	public static final String ADD_CUSTOMER_TO_ORDER_REQUEST = "order.request.new.SET_CUSTOMER";
	public static final String ORDER_REQUEST_READY = "order.request.new.READY";
	public static final String PAYMENT_REQUESTED = "order.payment.REQUEST";
	public static final String PAYMENT_SUCCESSFUL = "order.payment.SUCCESS";
	public static final String ORDER_CONFIRMED = "order.CONFIRMED";
}
