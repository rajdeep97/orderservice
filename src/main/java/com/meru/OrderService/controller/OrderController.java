package com.meru.OrderService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meru.OrderService.model.Order;
import com.meru.OrderService.service.OrderAPIService;

@RestController
public class OrderController {

	@Autowired
	private OrderAPIService service;
	
	@PostMapping("order/request/{customerID}")
	public ResponseEntity<String> placeOrder(@PathVariable long customerID)
	{
		this.service.placeOrderRequest(customerID);
		String response="Request to place order for Customer ID( "+customerID+" ) accepted.";
		return new ResponseEntity<String>(response,HttpStatus.ACCEPTED);
	}
	
	@GetMapping("order/search/{customerID}")
	public List<Order> getOrderByCustomerId(@PathVariable long customerID)
	{
		return this.service.getOrderByCustomerId(customerID);
	}
	
	@GetMapping("order/{orderID}")
	public Order getOrderByOrderId(@PathVariable long orderID)
	{
		return this.service.getOrderByOrderId(orderID);
	}
}
