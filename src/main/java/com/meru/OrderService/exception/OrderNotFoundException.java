package com.meru.OrderService.exception;

public class OrderNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3888581711000683737L;
	
	public OrderNotFoundException(String exceptionMessage)
	{
		super(exceptionMessage);
	}

}
