package com.meru.OrderService.exception;

public class UnableToProcessOrderException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8645993136488705812L;
	
	public UnableToProcessOrderException(String exceptionMessage)
	{
		super(exceptionMessage);
	}

}
